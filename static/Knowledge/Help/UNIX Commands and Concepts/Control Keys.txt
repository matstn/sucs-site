<p>There are many control key combinations (where you hold down the Ctrl key and press another key) that you will find useful both when using the shell and navigating text. These are listed in the table below.</p>
<table border="1">
<tbody>
<tr>
<th>Ctrl-</th><th>Function</th>
</tr>
<tr>
<td>a</td>
<td>Home - move cursor to the beginning of the line</td>
</tr>
<tr>
<td style="vertical-align: top;">c<br /></td>
<td style="vertical-align: top;">Cancel - abort <br /></td>
</tr>
<tr>
<td style="vertical-align: top;">d<br /></td>
<td style="vertical-align: top;">End of File - used to quit some programs cleanly<br /></td>
</tr>
<tr>
<td style="vertical-align: top;">e<br /></td>
<td style="vertical-align: top;">End - move cursor to the end of the line<br /></td>
</tr>
<tr>
<td style="vertical-align: top;">h<br /></td>
<td style="vertical-align: top;">Backspace<br /></td>
</tr>
<tr>
<td style="vertical-align: top;">k<br /></td>
<td style="vertical-align: top;">Delete from cursor to the end of the line<br /></td>
</tr>
<tr>
<td style="vertical-align: top;">q<br /></td>
<td style="vertical-align: top;">Start Transmitting - unfreezes output from the terminal<br /></td>
</tr>
<tr>
<td style="vertical-align: top;">r<br /></td>
<td style="vertical-align: top;">Reverse intelligent search - find the most recent match from the command history (works in bash and Milliways)<br /></td>
</tr>
<tr>
<td style="vertical-align: top;">s<br /></td>
<td style="vertical-align: top;">Stop Transmitting - freezes output from the terminal<br /></td>
</tr>
<tr>
<td style="vertical-align: top;">w<br /></td>
<td style="vertical-align: top;">Delete word at cursor<br /></td>
</tr>
<tr>
<td style="vertical-align: top;">z<br /></td>
<td style="vertical-align: top;">Suspend - stop the current job and return to the shell<br /></td>
</tr>
<tr>
<td style="vertical-align: top;">\<br /></td>
<td style="vertical-align: top;">Quit - quit a program (for example, if Ctrl-c doesn't work)<br /></td>
</tr>
</tbody>
</table>