<p>The Computer Society allows its members to keep their own sets of personal web pages, which can be accessed from anywhere on the Internet. Setting up your web pages is quite straightforward - this guide will show you how to do it.</p>

<h2>Setting up a directory</h2>

<p>Your web pages should be stored in a directory called <tt>public_html</tt>,
within your home directory. To create this directory, <a href="/Knowledge/Help/SUCS%20Services/Logging%20in%20remotely">SSH to silver</a>, then type the following command from the command-line prompt:</p>

<p><tt>mkdir ~/public_html</tt></p>

<p>The ~ (or &quot;tilde&quot;) character is usually obtained by pressing SHIFT and the key to the
left of the enter key on the third row of the keyboard. On a Mac keyboard, you will find this key immediately to the left of Z.</p>

<p>The above command will <strong>m</strong>a<strong>k</strong>e the <strong>dir</strong>ectory called <tt>public_html</tt>, the <tt>~/</tt> denoting that it is within your home
directory. To change to this directory, type:</p>

<p><tt>cd ~/public_html</tt></p>

<p>or just <tt>cd public_html</tt> if you are in your home directory already.</p><h2>Creating a file</h2>Web pages are written in <strong>HTML</strong> format - HTML stands for <strong>H</strong>yper<strong>T</strong>ext <strong>M</strong>arkup <strong>L</strong>anguage. To enable the computer to distinguish them from other files, each HTML filename must end with the
characters <tt>.html</tt> (note the full stop).

<p>It&#39;s wise to call your main page <tt>index.html</tt>. This is because unless a web browser is told which page within your directory to look at, it usually shows your <tt>index.html</tt> page. Also, you must have an <tt>index.html</tt> page for your website to be listed on your <a href="/Community/Members">Member page</a>.</p>

<p>You should create your file in a text editor. Many different editors are available on
our system, the most popular ones, in increasing order of friendliness, being
<tt>vi</tt>, <tt>joe</tt> and <tt>nano</tt>. Nano is probably the best editor
for beginners to use, as all the commands are listed at the bottom of the
screen at all times.</p>

<p>To edit a file called <tt>index.html</tt> in Nano, just type</p>

<p><tt>nano index.html</tt>&nbsp;</p>

<p>The convention <tt>^C</tt> at the bottom of the screen in Pico indicates that
you should hold the Control (Ctrl) key down and press the character noted.
For example, Control-X will exit Pico, and Control-O (the letter &#39;oh&#39;)
will save your current file (Nano calls this &#39;WriteOut&#39;).</p>

<h2>Writing your web page</h2>

<p>A HTML document is a text file, with special formatting, links and the like
marked by <strong>tags</strong>. The vast majority of tags come in pairs. The initial
tag, <tt>&lt;tag&gt;</tt> turns on an effect, the final tag, <tt>&lt;/tag&gt;</tt>
turns it off. With the exception of the &quot;DOCTYPE&quot;, tags should be written in lower case.</p>

<p>With that in mind, let&#39;s look at a simple example of HTML. You can 
<a href="/files/Help/example.html">view this page</a> (press Back on your browser to
return here when you&#39;re done).</p>

<pre>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.01//EN&quot; &quot;http://www.w3.org/TR/html4/strict.dtd&quot;&gt;<br />&lt;html&gt;<br />   &lt;head&gt;<br />      &lt;title&gt;My Homepage&lt;/title&gt;<br />   &lt;/head&gt;<br />   &lt;body&gt;<br />      &lt;h1&gt;Hello, this is my page&lt;/h1&gt;<br />      &lt;p&gt;I&#39;m a student at &lt;strong&gt;Swansea University.&lt;/strong&gt;&lt;/p&gt;<br />      &lt;p&gt;I&#39;m also a member of &lt;a href=&quot;http://www.sucs.swan.ac.uk/&quot;&gt;SUCS&lt;/a&gt;&lt;/p&gt;<br />   &lt;/body&gt;<br />&lt;/html&gt;</pre>

<p>The tag that starts <tt>&lt;!DOCTYPE</tt> tells the web browser which of the HTML standards to follow. In this case, we are using HTML 4.01. The tag <tt>&lt;html&gt;</tt> then starts the HTML
document. All HTML files are divided into two sections - a <strong>head</strong> and a <strong>body</strong>.</p>

<p>The head of the file always starts with the tag <tt>&lt;head&gt;</tt>, and finishes
with the tag <tt>&lt;/head&gt;</tt>. The head can contain general information about the file: its author, subject matter, copyright status and so on. In this case, all we&#39;ve put in the head is the title of the page (between the beginning and closing tags <tt>&lt;title&gt;</tt> and <tt>&lt;/title&gt;</tt>). The title of the page will normally be displayed at the top of the browser window, next to the browser name.</p>

<p>The body of the file now follows. The first line of the body shows an example of a <strong>heading</strong>. Heading size 1, denoted by the tags <tt>&lt;h1&gt;...&lt;/h1&gt;</tt> is the largest size of heading available in the browser. Text marked with <tt>&lt;h1&gt;</tt> will normally be written in bold type, significantly larger than normal text.</p>

<p>A normal line of text then follows, starting with a <tt>&lt;p&gt;</tt> tag, indicating that this is a paragraph. If you&#39;ve viewed the <a href="/files/Help/example.html">example file</a>, you&#39;ll have noticed that the text &quot;Swansea University&quot; appears in <strong>bold</strong>. This
is accomplished by the tags <tt>&lt;strong&gt;...&lt;/strong&gt;</tt> at the beginning and end
of the phrase. Other text effects are also available - we&#39;ll return to these.</p>

<p>This line of text finishes with the tag <tt>&lt;/p&gt;</tt>, indicating that the paragraph has finished. The browser prints a blank line between this and the next line of text. Note that the web browser will <strong>not</strong> move on to the next line of text unless it finds a specific tag telling it to do so - it ignores the position of new lines in your HTML file.</p>

<p>The next line shows the main feature of HTML: the ability to create links
between one document and another. The code for this is</p>

<p><tt>&lt;a href=&quot;location of other page&quot;&gt;...&lt;/a&gt;</tt></p>

<p>The location of the page to be linked is given in the quotes following <tt>href=</tt>. This should normally be of the format <tt>http://...</tt> that Firefox and other browsers show just below the menu bar. However, if you&#39;re linking a page on the same directory, you can just give the filename within the quotes (or a path to a subdirectory if the file is within the subdirectory).</p>

<h2>Locating your files and letting others see them</h2>

<p>Before anyone can read your page using a browser (this includes you too, generally) you <strong>must</strong> make sure that everybody is given permission to read the files in your <tt>public_html</tt> directory. To do this, type the following four commands from the command line, pressing Enter after each:</p>

<pre>cd<br />chmod a+x .<br />cd public_html<br />chmod -R a+rx .<br /></pre>

<p>(For the curious, these commands change to your home directory, give all users execute permission on your home directory, change to your <tt>public_html</tt> directory, and recursively give all users read and execute permission on all files and subdirectories within this  directory).</p>

<p>Your web address will be of the form:</p>

<p><tt>http://sucs.org/~<em>username</em>/</tt></p>

<p>where <em><tt>username</tt></em> should be replaced by the username you chose for your SUCS account. This assumes that you have a file called <tt>index.html</tt> in your <tt>public_html</tt> directory: if not, you&#39;ll have to add the filename of your index page to the end of the location above.</p>

<p>So if your username is <tt>spod</tt>, and you wish to access the file called
<tt>bobbins.html</tt> in your <tt>public_html</tt> directory, you would use:</p>

<p><tt>http://sucs.org/~spod/bobbins.html</tt></p>

<h2>Summary</h2>

<p>Having followed this guide, you should now have a folder called <tt>public_html</tt> in your SUCS account home directory, in which you have created a text file called <tt>index.html</tt> containing a valid HTML document. They should have been given the correct permissions so that when visiting <tt>http://sucs.org/~<em>username</em>/</tt> you are able to see the web page you created. </p><p>If it hasn&#39;t worked, don&#39;t worry - help is at hand! Log on to <a href="/Knowledge/Help/SUCS%20Services/Using%20Milliways">Milliways</a> and ask for help. There&#39;s nearly always someone there ready to offer assistance. Alternatively, post a message on the <a href="/Community/Forum/">Forum</a>.<br /></p>

<h2>Credits&nbsp;</h2><p>Current version by <a href="http://sucs.org/~dez/">Dez</a>, based on the guide written by <a href="http://sucs.org/~rhys/">Rhys</a> which was inspired by a guide to HTML on the Merton College, Oxford website, written by
Robin Stevens.</p>