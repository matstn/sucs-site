<h1>This page is out of date, and the instructions on it are unlikely to work. &nbsp;</h1><p>
The Computer Society offers you extra disk space, and the chance to use this
disk space directly from your campus account. This can be done using the SMB
protocol. SUCS runs a server called Samba, which is available at
<strong>sucssmb</strong>. For most users the DAV option described above is easier
now that almost all the campus machines support DAV.&nbsp;</p><h3>Getting an account</h3>
In order for you to use this service, you must have an account created by a member of <a href="../../../About/Staff">staff</a>. Once you have an account, follow the instructions below.&nbsp;<h3>Connecting</h3>

<p>To connect to the server, download
<a href="oa/mapsucs.bat">mapsucs.bat</a> and run it
from the Command Prompt (Start, Programs, Command Prompt) with your username
as a parameter:&nbsp;</p>

<table border="0" bgcolor="#0000aa">
  <tbody><tr><td>
    <font color="#ffff00"><strong>&gt; mapsucs <em>username</em></strong></font>
  </td></tr>
</tbody></table>

<p>You will be prompted for your <em>Samba</em> password three times - once for every drive that will be mapped. The drives are mapped as follows:</p>

<table border="1">
  <tbody><tr><th>
Path
  </th><th>
Drive letter
  </th><th>
Description
  </th></tr>

  <tr><td>
sucssmbhomes
  </td><td>
h:
  </td><td>
Your personal disk space on SUCS
  </td></tr>

  <tr><td>
sucssmbwinapps
  </td><td>
i:
  </td><td>
Windows Applications installed on SUCS
  </td></tr>

  <tr><td>
sucssmbftp
  </td><td>
j:
  </td><td>
The SUCS Anonymous FTP site
  </td></tr>
</tbody></table>

<p>Once the drives have been mapped once, a record of
this is made in your personal Windows Registry settings. This means
that your drives will be automatically mapped every time you log on. If
for any reason your settings are lost, all you have to do is run the
batch file again to recreate the mappings.&nbsp;</p><h3>Changing your password</h3>
To change your password, ssh to sucs, and use the command smbpasswd.
You will be asked for your current password, then prompted for your new
one twice, to confirm that you have typed it correctly.&nbsp;<h3>Deleting files</h3>
You must be careful when deleting files on SUCS over a Samba connection. 
The files will not be transferred to the Recycle Bin. You should not delete symbolic links to directories with your Samba connection as the files in the 
real directory will be deleted too. It is strongly recommended that you
always use the Linux <strong>rm</strong> command to remove such files.