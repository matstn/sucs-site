
<p>Computer Society members all receive email accounts of the form
<em>username</em>@sucs.org. This is separate to your university email
account. </p><h3>Reading your SUCS Mail <br /></h3><p>You can access your mail <a href="../../../webmail">via the
web</a>, directly from mail clients on the SUCS machines, or elsewhere using any software compatible with secure POP3/IMAP. </p>

<p>Mozilla Thunderbird is a good mail client which is supplied on the  <span style="font-style: italic">SUCS Members&#39; CD</span>. There is a walkthrough for configuring Thunderbird <a href="Accessing%20your%20email/Configuring%20Thunderbird">here</a>.</p>

<h3>Mail server details <br /></h3><p>If you need to set up a different mail client, the relevant details are as follows:</p>

<table border="0">
<tbody><tr><td>Protocol</td><td>secure IMAP or secure POP3 (via SSL) </td></tr>
<tr><td>Server</td><td>sucs.org</td></tr>
<tr><td>Username</td><td><em>your username</em></td></tr>
<tr><td>Password</td><td><em>your password</em></td></tr>
</tbody></table>

<p>The society uses a single sign-on system, so if you change your password for
WebDAV, login or email, the others follow.</p>