<!-- Joining box --> 
<div class="cbb"> 
<h3>Join SUCS!</h3> 
<p>As well as being an excellent way to meet people with similar interests (we have regular social events and we&#39;re all really nice), you also get all this great stuff: </p> 
<ul> <li>Use of our dedicated <a href="../Games/" title="Gameserver">games server</a></li> 
<li><a href="../Knowledge/Help/SUCS%20Services/Using%20your%20web%20space">Web hosting</a> with scripting and database support</li>
<li>Free printing*</li> 
<li>Disk space</li> 
<li>Use of our <a href="../Knowledge/Library/" title="Library">library</a> of textbooks</li> 
<li>Much more!</li> </ul> 
<div style="text-align: right"><a href="../About/Joining" title="More info on joining SUCS">More Info</a> </div></div>  
<!-- Contact box --> 
<div class="cbb"> 
<h3>Contact Us</h3>
<p>If you&#39;re having any problems, you can get in touch with us by e-mailing</p>
<a href="mailto:admin@sucs.org">admin@sucs.org</a> 
</div>
