<div class="box">
	<div class="boxhead"><h2>James Frost (frosty) - What's A Linux?</h2></div>
	<div class="boxcontent">
	<p>James gives a brief overview of Linux: what is it, where to get it from, and how to install it.</p>

<div id="player"><object type="application/x-shockwave-flash" data="/videos/talks/mediaplayer.swf?file=2007-11-07/2007-11-07-frosty.flv" width="320" height="260" allowscriptaccess="always" allowfullscreen="true" file="/videos/talks/2007-11-07/2007-11-07-frosty.flv" image="/videos/talks/2007-11-07/2007-11-07-frosty.png" id="player" displayheight="240"><param name="height" value="240" /><param name="width" value="320" />
<param name="file" value="/videos/talks/2007-11-07/2007-11-07-frosty.flv" />
<param name="image" value="/videos/talks/2007-11-07/2007-11-07-frosty.png" />
<param name="id" value="player" />
<param name="displayheight" value="240" />
<param name="FlashVars" value="image=/videos/talks/2007-11-07/2007-11-07-frosty.png" />
</object></div>

<p><strong>Length: </strong>10m 22s</p>
<p><strong>Video:</strong>Coming Soon (H.264 .mov)</p>
<p><strong>Slides:</strong>Coming Soon (PDF)<br /></p>
</div>
	<div class="boxfoot"><p>&nbsp;</p></div>
</div>
