			<div id="loginForm">
				<form method="post" action="{$ssl_url|escape:'htmlall'}{getparams gets=$gets}">
					<div>
{if $session->loggedin}
	Hello <a href="{$baseurl}/Community/Members/{$session->username}">{$session->username}</a>! You have 
	{if $session->email_forward}
		mail forwarded to {$session->email_forward}<br />
	{else}
		{if $session->email==0}
			no new email
		{elseif $session->email==1}
			<a href="https://sucs.org/webmail/">new email</a>
		{/if}
	<br />
	Print balance: {$session->printbalance} | <a href="{$baseurl}/Options">Membership Options</a>
	<br />
	{/if}
	<input type="submit" class="button" name="Logout" id="Logout" value="Logout" />
{else}
					<input type="text" class="text" name="session_user" id="session_user" placeholder="Username" />
					<input type="password" class="text" name="session_pass" id="session_pass" placeholder="Password" />
					<input type="hidden" name="token" value="{$session->token}" />
					<input type="submit" class="button" name="Login" id="Login" value="Login" />
					<br>
					<a href="{$baseurl}/join">No Account? Don't worry, sign up today!</a>
{/if}
					</div>
				</form>
			</div>
