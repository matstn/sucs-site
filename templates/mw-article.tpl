<div id="punwrap">
<div id="punForum" class="pun">

{if $mode == "reply"}
{include file="mw-new.tpl"}
{/if}

<div class="linkst">
   <div class="inbox">
     <p class="postlink conr"><a href="{$path|escape:'html'}/reply">Post Reply</a></p>
     <p class="pagelink">Subject: {$message.subject|escape:'html'}</p>
     <div class="clearer"></div>
   </div>
</div>

<div id="p{$message.ref}" class="blockpost rowodd firstpost">
 <h2><span><span class="conr">#{$message.ref}&nbsp;</span>{$message.date|date_format:"%H:%M:%S %A, %B %e %Y"}</span></h2>
 <div class="box">
  <div class="inbox">
   <div class="postleft">
    <dl>
     <dt><strong>{$message.from|escape:'html'}</strong></dt>
     <dd>To: {$message.to|escape:'html'}</dd>
{if $message.replyto}
     <dd>Reply To: #{$message.replyto}</dd>
{/if}
    </dl>
   </div>
   <div class="postright">
    <h3>{$message.subject|escape:'html'}</h3>
    <div class="postmsg">
	    <p>{$message.body|escape:'html'|nl2br}</p>
    </div>
    <div class="clearer"></div>
   </div>
  </div>
 </div>
</div>

{if count($articles) > 0}
There are {$howmany} replies
{foreach from=$articles item=message}
<div id="p{$message.ref}" class="blockpost roweven">
 <h2><span><span class="conr">#{$message.ref}&nbsp;</span>{$message.date|date_format:"%H:%M:%S %A, %B %e %Y"}</span></h2>
 <div class="box">
  <div class="inbox">
   <div class="postleft">
    <dl>
     <dt><strong>{$message.from|escape:'html'}</strong></dt>
     <dd>To: {$message.to|escape:'html'}</dd>
{if $message.replyto}
     <dd>Reply To: #{$message.replyto}</dd>
{/if}
    </dl>
   </div>
   <div class="postright">
    <h3>{$message.subject|escape:'html'}</h3>
    <div class="postmsg">
	    <p>{$message.body|escape:'html'|nl2br}</p>
    </div>
    <div class="clearer"></div>
    <div class="postfootright"><a href="{$shortpath}/{$message.ref}/reply">Reply</a></div>
   </div>
  </div>
 </div>
</div>



{/foreach}
{/if}



</div>
</div>
