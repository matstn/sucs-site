<div>
{literal}
<style type="text/css">
 .nicebox {
	background: none;
	border: 0px;
	border-bottom: dotted thin black;
	margin: 5px;
 };
</style>
{/literal}
<form method="post" action="{$path}/post">
To: <input class="nicebox" type="text" name="to" size="40"{if $mode=="reply"} value="{$message.from|escape:'html'}"{/if}/><br />
Subject: <input class="nicebox" type="text" name="subject" size="40"{if $mode=="reply"} value="Re: {$message.subject|escape:'html'}"{/if}/><br />
<br />
<textarea name="body" cols="80" rows="10"></textarea><br />
<input type="submit" name="submit" value="Post Comment" />
</form>
</div>
