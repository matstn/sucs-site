{if $mode == 'browse'}
<p>SUCS has a wide variety of computer and computer science related books available for loan to our members. If you'd like to borrow a book, speak to a member of admin or the exec.</p>

<h3>Browse by category</h3>
<ul>
{foreach name=tags from=$tags item=tag}
<li><a href="{$baseurl}/Knowledge/Library/Tags/{$tag.name|escape:'url'}">{$tag.name}</a></li>
{/foreach}
</ul>
<h3>Random Books</h3>
	{foreach name=randoms from=$randoms item=randomitem}
	<a href="{$baseurl}/Knowledge/Library/{$randomitem.id}"><img src="{$randomitem.image_url}" alt="{$randomitem.title}" height="120" /></a>
	{/foreach}

{elseif $mode == 'display'}

{include file='library-book.tpl' book=$book}

{elseif $mode == 'search'}
<h3>Search Results</h3>
	{if $results|@count < 1}<p>No results found</p>
	{else}
	<ul>
	{foreach name=results from=$results item=result}
<li><a href="{$baseurl}/Knowledge/Library/{$result.id}">{$result.title}</a>{if $result.onloan} <em>(on loan)</em>{/if} </li>
	{/foreach}
	</ul>
	{/if}
{elseif $mode == 'tagdisplay'}
	{foreach name=results from=$results item=result}
		<li><a href="{$baseurl}/Knowledge/Library/{$result.id}">{$result.title}</a> </li>
	{/foreach}
{elseif $mode == 'bookerror'}
	<p>The requested book does not exist</p>
{elseif $mode == 'tagerror'}
	<p>The requested tag does not exist</p>
{/if}
{if $librarian == true}
<p><a href="{$baseurl}/Knowledge/Library/Admin">Library Admin</a></p>
{/if}
<p>Images and descriptions provided by <a href="http://www.amazon.co.uk">Amazon</a></p>
