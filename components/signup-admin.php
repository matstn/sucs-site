<?php
// Config options
$permission="sucsstaff";
// TODO: DEV: UNSTICK THIS BEFORE DEPLOY
if (isset($session->groups[$permission])) {
	$smarty->assign("staff",TRUE);

	//get unused signup slips
	$query  = "SELECT signup.id, signup.sid, member_type.typename, members.username, signup.card";
       	$query .= " FROM signup LEFT JOIN member_type ON signup.type = member_type.id LEFT JOIN members ON signup.issuedby=members.uid";
	$query .= " WHERE";
	$query .= " signup.activated is NULL";
	$query .= " or signup.username is NULL";
	$query .= " ORDER BY signup.id";
	$query .= ";";
	$data = $sucsDB->GetAll($query);
        // process responces
        if(isset($_POST['command'])){
                if($_POST['command']=='update'){
			$changed = 0;
			$upquery  = "UPDATE signup";
                        $upquery .= " SET sid = ?";
			$upquery .= " WHERE id = ?";
			$upquery .= ";";
			foreach($data as $value){
				if(array_key_exists('sid:'.$value['id'],$_POST) && $_POST['sid:'.$value['id']]!=$value['sid']){
					$uparray = array($_POST['sid:'.$value['id']],$value['id']);
					$sucsDB->query($upquery,$uparray);
					$changed++;
				}
			}
                        message_flash($changed." record(s) updated");

                }
	}
	$data = $sucsDB->GetAll($query);
	//set smarty stuff
	$smarty->assign("signups",$data);
	$smarty->assign("self",$baseurl.$path.$getout);
}
$body = $smarty->fetch("signup-admin.tpl");
$smarty->assign('title', "Signup Slip Admin");
$smarty->assign('body', $body);
?>
