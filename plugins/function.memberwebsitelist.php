<?php


function smarty_function_memberwebsitelist($params, &$smarty) {

	$list = $params['members'];
	$letter = "";

	foreach ($list as $item) {
		if ($letter != strtoupper(substr($item['uid'], 0, 1))) {
			$letter = strtoupper(substr($item['uid'], 0, 1));
		}
		$members[$letter][] = $item['uid'];
	}

	foreach ($members as $letter => $lettermembers) {
		$memcount = sizeof($lettermembers);
		$col1count = round($memcount / 2);
		$col1height = $col1count * 1.2;
		
		$output .= "<h4>$letter</h4>\n<ul>\n";

		for ($i = 0; $i < $memcount; $i++) {		
			$member = $lettermembers[$i];

			if ($i < $col1count) {
				$output .= "<li class=\"column1\">";
			} elseif ($i == $col1count) {
				$output .= "<li class=\"column2\" style=\"margin-top: -{$col1height}em\">";
			} else {
				$output .= "<li class=\"column2\">";
			}

			$output .= "<a href=\"http://sucs.org/~$member\">$member</a></li>\n";
		}

		$output .= "</ul>\n";
		
	}

	return $output;
}

?>
