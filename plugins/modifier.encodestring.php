<?php
	function smarty_modifier_encodestring($string) {
		$string = rawurlencode($string);
		$string = str_replace("_", "%5F", $string);
		$string = str_replace("%20", "_", $string);
		return $string;
	}
?>
