<?
require_once("validationData.php");
require_once("sanitization.php");
/* useful validation functions */

//check for a valid email address
function validEmail ($email)
{
	global $error;
	//split user and domain
	list($user,$domain) = explode("@", $email);
	// check for bad characters, and check for zero length user & domain
	if(!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i",$email) or !$user or !$domain )
	{
		$error = 'an invalid email address (syntax)';
		return false;
	}
	// Syntax OK
	
	// Check for an mail server
	elseif(!getmxrr($domain,$mx) or !gethostbyname($domain)) 
	{
		$error = "no mail servers listed for '$domain'";
		return false;
	}
	else 
	{
		// Email address valid from technical point of view
		return true;
	}
}

// test whether a password is considered Strong Enough
// ideally we'd want to use cracklib or something here, but no RPM for the php bindings :-(
// dont use this, use weakPassword instead it uses cracklib
function strongPassword ($pass) {

	// you call this a password? my cat could bruteforce this.
	if (strlen($pass) < 6) {
		return false;
	}
	
// start at 0, and increment for certain features
	$score = 0;


// greater than 8 characters
	if (strlen($pass) > 8) $score++;	
// includes lowercase characters
	if (preg_match("/[a-z]/", $pass)) $score++;
// includes uppercase characters
	if (preg_match("/[A-Z]/", $pass)) $score++;
// includes digits
	if (preg_match("/\d/", $pass)) $score++;
// includes "non-word" characters
	if (preg_match("/\W/", $pass)) $score++;

// I reckons if it has at least 3 of the above it should be... adequate
// better if it checked for dictionary words too though
	if ($score > 3) {
		return true;
	} else {
		return false;
	}
}

# Use cracklib to check for weak passwords.
# returns FALSE if the password is good i.e. not weak
# otherwise returns a string saying why its weak
function weakPassword($password)
{
	// Try fedora then debian known paths
	if (file_exists("/usr/sbin/cracklib-check"))
		$cracklib = "/usr/sbin/cracklib-check";
	else
	if (file_exists("/usr/sbin/crack_testlib"))
		$cracklib = "/usr/sbin/crack_testlib";
	else
		return "Cannot find cracklib";

	$proc = proc_open($cracklib, array(0=>array("pipe","r"),1=>array("pipe","w")),$pipes,'/tmp/',NULL);
	if (!is_resource($proc)) {
		return "Cannot find cracklib";
	}
	fwrite($pipes[0], $password);
	fclose($pipes[0]);
	$last = "";
	do {
		$last = fgets($pipes[1]);
		if ($last !== FALSE) $answer = trim($last);
	} while ($last !== FALSE);
	fclose($pipes[1]);
	proc_close($proc);
	$answer = substr(strrchr($answer,":"),2);
	if (strtolower($answer) == "ok") return FALSE;
	if ($answer == "") return("Empty password");
	return $answer;
}
// check if username is an alias
function isAlias($username){
        $ok=false;
        // check its not an alias
        $aliasesfile = file ('/etc/aliases');
        foreach ($aliasesfile as $aliasline)
        {
                if(trim($aliasline) && $aliasline[0]!="#")
                {
                        $anAlias = explode(":", trim($aliasline));
                        if($anAlias[0] && !posix_getpwnam($anAlias[0]) && ($anAlias[0] == $username)){
                                $ok=true;
                                return true;
                        }
                }
        }
	return $ok;
}
//check if a user with a sid already exsists
function sidUsed($sid){
	$sucsDB = NewADOConnection('postgres8');
	$sucsDB->Connect('dbname=sucs');
	$sucsDB->SetFetchMode(ADODB_FETCH_ASSOC);
	$query = "SELECT * FROM members WHERE sid=?";
	$data = $sucsDB->GetAll($query,$sid);
	return (sizeof($data) > 0);
}


function validUsername ($username){
	global $error;
	// check if uname is sytactically valid
	$syntax = preg_match("/^[a-z][a-z0-9_]*$/", $username);
	if(!$syntax || (strlen($username) < 2)){
		$error = "Usernames must start with a letter, only contain lowercase letter, numbers 0-9 and underscores (_) and be at least 2 characters.";
		return false;
	}
	// check if the username already exsists
	elseif(posix_getpwnam($username))
        {
		$error = "Username already taken";
                return false; 
	}
	// check if its a mail alias
	elseif(isAlias($username)){
        	$error ="Username is a mail alias";
        	return false;
	}	
	else{
		return true;
	}
}
function validSID($SID,$override){
	global $error;
	if($override){
		if($SID==""){
			$error = "You MUST provide some sort of student number";
			return false;
		}
		else{
			return true;
		}
	}
	else{
		if(!preg_match("/^[0-9]*$/", $SID) || strlen($SID) != 6){
			$error = "Invalid student ID";
			return false;
		}
		elseif(sidUsed($SID)){
			$error = "A user with that student ID already exists, email <a href=\"mailto:admin@sucs.org\">admin@sucs.org</a> if this is an error.";
			return false;
		}
		elseif(lookupSID($SID)==" "){
			$error = "Student not found, email<a href=\"mailto:admin@sucs.org\">admin@sucs.org</a> if this is an error.";
			return false;
		}
		else{
			return true;
		}
	}
}
function validRealName($realName,$override){
	global $error;
	if($override){
		if($realName==""){
			$error="You MUST provide some sort of name";
			return false;
		}
		else{
			return true;
		}
	}
	else{
	        //check for enough names for real name (we insist on at least 2
	        if(count(explode(" ",$realName)) < 2)
	        {
			$error = "Too few names given, please give at least two.";
			return false;
	        }
	        //check for a sane realname, see comment below
		elseif (!preg_match("/^([A-Z]([.]+ +[A-Z])*([\']+[A-Z])*[a-z]+[ -]*)+$/", $realName))
	        {
			$error = "Name incorrectly formatted, email <a href=\"mailto:admin@sucs.org\">admin@sucs.org</a> if this is an error.";
			return false;
	        }
/* 
 * This should force sane real names, with capitals for the first letter of each word,
 * Whist alowing for complex names such as Robin M. O'Leary
 * 
 * break down of regexp
 * 
 * (
 * [A-Z]                - start with a single capital
 * ([.]+ +[A-Z])*       - zero or more of, (at least one "." followed by at least one space then another single capital)  //we dont expect people to have initals at the end of there names so this is alright
 * ([\']+[A-Z])*        - zero or more of, (at least one "'"s followed by a single capital letter)
 * [a-z]+               - One or more lower case letters, this forces initals to be followed by a "."
 *[ -]*           - zero or more " "s or "-"s so double barreled names are supported
 * )
 * 
 * In its current state 
 * Robin M. O'Leary is valid
 * Robin M O'Leary is not
 * Robin M. OLeary is Not
 * Robin M. O'LeaRy is valid (though its not meant to be.. bad side effect of not requireing at least one space...)
 * BUT... this alows for McSmith's... which is rather nice :)... and of course delibrate
 * RObin M O'Leary is not
 *
 */	
		else{
			return true;
		}
	}
}
function validSocName($socname,$override){
	global $error;
	if($override){
		if($socname==""){
			$error="You MUST provide some sort of name";
			return false;
		}
		else{
			return true;
		}
	}
	else{
		if(!preg_match('/^[A-Z1-9]/',$socname) || strlen($socname) < 2){
			$error = "Must start with a capital letter or a number and be more than 1 character";
			return false;
		}
		else{
			return true;
		}
	}
}

function validAddress($address){
	global $error;
    $address = sanitizeAddress($address);
    $regex="/^([A-Z0-9]([[:alnum:]]|[ .\/'-])*\n)+[A-Z0-9]([[:alnum:]]|[ .\/'-])*$/";
        if(!preg_match($regex,$address)){
                $error = "Please supply at least two valid lines of address.";
                return false;
        }   
        else{   
                return true;
        }
}

function validPhone($phone){
	global $error;
	$phone=sanitizePhone($phone);
	if(!preg_match("/^\+?[0-9-]+$/",$phone)){
		$error = "Must be all numbers";
		return false;
	}
	return true;
}
function validSignupEmail($email){
	global $error;
	if(preg_match('/@sucs\.org$/',$email)){
		$error = "SUCS email addresses are not allowed";
		return false;
	}
	elseif(!validEmail($email)){
		return false;
	}
	else{
		return true;
	}
		
}
?>
