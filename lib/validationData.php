<?
// lookup real names from sid's useing campus ldap
function lookupSID($sid) {
	$ds=ldap_connect("ccs-suld1.swan.ac.uk");
	$ldappw = file_get_contents("/etc/unildap.secret");
	$ldappw = trim($ldappw);
	ldap_bind($ds, "cn=SUCS-BIND,ou=ServiceAccount,o=SWANUNI",$ldappw );
	$sr=ldap_search($ds, "ou=students,ou=Swansea,o=swanuni", "uid=".$sid); 
	$info = ldap_get_entries($ds, $sr);
	ldap_unbind($ds);
	return ucwords(strtolower($info[0]['givenname'][0]." ".$info[0]['sn'][0]));
}

// lookup addresses from postcodes useing the univeritys website
function lookup_postcode($postcode) {

	include_once "../paf-key.php";

        $url = "http://paf.sucs.org/?apikey=$apikey&postcode=".rawurlencode($postcode);

        $req = curl_init($url);
        $page = curl_exec($req);
        curl_close($req);

}
?>
